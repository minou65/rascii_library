/*  Arduino Library for the Rocrail RASCII protocol.
Copyright (c) 2018 Andreas Zogg	<don_funk@bluewin.ch>
Copyright (c) 2016 Rob Versluis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Rob Versluis Feburary 14, 2016
Forked by Andreas Zogg, September 26, 2018.

RASCII-Arduino - Version 1.0.1
*/

// uncomment the following lin for Debug Messages
// #define RASCII_DEBUG_MSG

#include <SoftwareSerial.h>

#include "RASCII.h"



SoftwareSerial* RS485Serial;

RASCII::RASCII(bool softserial, uint8_t rxPin, uint8_t txPin, uint8_t txCtrl) {
	m_bSoftwareSerial = softserial;
	m_iSoftSerialRX = rxPin;
	m_iSoftSerialTX = txPin;
	m_iSoftSerialTxControl = txCtrl;
#ifdef RASCII_EXTENDED_FUNCS
	//Initialize Queue:
	memset(Queue, 0, sizeof(Queue));
#endif
}


void RASCII::begin(long int baudrate) {
	if (m_bSoftwareSerial) {
		RS485Serial = new SoftwareSerial(m_iSoftSerialRX, m_iSoftSerialTX);
		RS485Serial->begin(baudrate);
	}
	else {
		Serial.begin(baudrate);
		while (!Serial);
	}
}

bool RASCII::read(byte* cmd) {
	while (Serial.available()) {
		char c = (char)Serial.read();

		if (c == '@') {
			char buffer[32];
			char hexa[2];
			if (m_bSoftwareSerial)
				RS485Serial->readBytes(hexa, 2);
			else
				Serial.readBytes(hexa, 2);
			byte len = HexA2Byte(hexa);
			if (m_bSoftwareSerial)
				RS485Serial->readBytes(buffer, len);
			else
				Serial.readBytes(buffer, len);
			buffer[len] = '\0';
			for (byte i = 0; i < len; i++) {
				cmd[i] = HexA2Byte(buffer + i * 2);
			}
			return true;
		}
	}
	return false;
}

void RASCII::write(const char* remark) {
	byte len = strLen(remark);
	if (len > 0) {
		char hexa[3];
		if (m_bSoftwareSerial)
			RS485Serial->write(INF);
		else
			Serial.write(INF);
		if (m_bSoftwareSerial)
			RS485Serial->write(Byte2HexA(len, hexa), 2);
		else
			Serial.write(Byte2HexA(len, hexa), 2);
		if (m_bSoftwareSerial)
			RS485Serial->write(remark, len);
		else {
			Serial.write(remark, len);
			Serial.write("\n", 1);
		}
	}
}

void RASCII::write(byte evt, byte* data, byte len, byte type) {
	char hexa[3];
	// Type @,#,$,&
	if (m_bSoftwareSerial)
		RS485Serial->write(type);
	else
		Serial.write(type);

	// Length
	if (m_bSoftwareSerial)
		RS485Serial->write(Byte2HexA((len + 1) * 2, hexa), 2);
	else
		Serial.write(Byte2HexA((len + 1) * 2, hexa), 2);

	// Command, Event
	if (m_bSoftwareSerial)
		RS485Serial->write(Byte2HexA(evt, hexa), 2);
	else
		Serial.write(Byte2HexA(evt, hexa), 2);

	// Data
	for (byte i = 0; i < len; i++) {
		if (m_bSoftwareSerial)
			RS485Serial->write(Byte2HexA(data[i], hexa), 2);
		else
			Serial.write(Byte2HexA(data[i], hexa), 2);
	}
	if (!m_bSoftwareSerial)
		Serial.write("\n", 1);
}

byte RASCII::strLen(const char* s) {
	byte len = 0;
	while (s != NULL && s[len] != 0) {
		len++;
	}
	return len;
}

byte RASCII::HexA2Byte(const char* s) {
	byte h, l, val;
	if (s[0] >= 'A') h = s[0] - 55;
	else h = s[0] - 48;
	if (s[1] >= 'A') l = s[1] - 55;
	else l = s[1] - 48;
	val = l + (h << 4);
	return val;
}

char* RASCII::Byte2HexA(byte b, char* c) {
	static char cHex[] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F' };
	c[0] = cHex[(b & 0xF0) >> 4];
	c[1] = cHex[b & 0x0F];
	c[2] = '\0';
	return c;
}

#ifdef RASCII_EXTENDED_FUNCS

void RASCII::transmit(bool txrx) {
	digitalWrite(m_iSoftSerialTxControl, txrx);
}

void RASCII::queuePut(const char* remark) {
	byte len = strLen(remark);
	queuePut(0, (byte*)remark, len, INF);
}

void RASCII::queuePut(byte evt, byte* data, byte len, byte type) {
	for (byte i = 0; i < QUEUESIZE; i++) {
		if (Queue[i].type == 0) {
			Queue[i].type = type;
			Queue[i].evt = evt;
			Queue[i].len = len;
			memset(Queue[i].data, 0, sizeof(Queue[i].data));
			memcpy(Queue[i].data, data, len);
			break;
		}
	}
}

void RASCII::queueWrite() {
	for (byte i = 0; i < QUEUESIZE; i++) {
		if (Queue[i].type != 0) {
			if (Queue[i].type == INF)
				write((const char*)Queue[i].data);
			else
				write(Queue[i].evt, Queue[i].data, Queue[i].len, Queue[i].type);
			Queue[i].type = 0;
			for (byte n = i; n < QUEUESIZE; n++) {
				if (n + 1 >= QUEUESIZE)
					break;
				if (Queue[n + 1].type != 0) {
					Queue[n].type = Queue[n + 1].type;
					Queue[n].evt = Queue[n + 1].evt;
					Queue[n].len = Queue[n + 1].len;
					memcpy(Queue[n].data, Queue[n + 1].data, Queue[n + 1].len);
					Queue[n + 1].type = 0;
				}
			}
			break;
		}
	}
}

#endif

void RASCII::startRun() {
	_Run = true;
}

void RASCII::stopRun() {
	_Run = false;
}

void RASCII::process(byte boardaddr_) {
	byte cmd_[64];
	char s[32];

	// delay of 10ms
	if (millis() - _previousMillis >= 10) {
		_previousMillis = millis();

		if (read(cmd_)) {
			//byte Command_ = cmd_[0];
			//byte addr_ = cmd_[1];

#ifdef RASCII_DEBUG_MSG
			Serial.println(F("RASCII::process")); 
			Serial.print(F("  Addr  : "));
			Serial.print(cmd_[1], DEC);
			Serial.print(F(", Command: "));
			Serial.println(cmd_[0], DEC);
			Serial.print(F("  myAddr: "));
			Serial.println(boardaddr_);
#endif

			if (cmd_[1] == 0xFF || cmd_[1] == boardaddr_ || cmd_[0] == CMD_INFO) {
				switch (cmd_[0]) {
				case CMD_INFO:
					if (notify_CMD_INFO) {
						notify_CMD_INFO();
					}
					else {
						sprintf(s, "RocDino V0.1 addr=%d", boardaddr_);
						write(s);
					}
					break;

				case CMD_START:
					write("Start reading");
					_Run = true;
					break;

				case CMD_STOP:
					write("Stop reading");
					_Run = false;
					break;

				case CMD_DOUT:
					if (notify_CMD_DOUT) notify_CMD_DOUT(cmd_);
					break;

				case CMD_AOUT:
					if (notify_CMD_AOUT) notify_CMD_AOUT(cmd_);
					break;

				case CMD_GETCONF:
					if (notify_CMD_GETCONF) notify_CMD_GETCONF();
					break;

				case CMD_SETCONF:
					if (notify_CMD_SETCONF) notify_CMD_SETCONF(cmd_);
					break;

				case CMD_RESETCONF:
					if (notify_CMD_RESETCONF) notify_CMD_RESETCONF();
					break;

				case CMD_GETCV:
					if (notify_CMD_GETCV) notify_CMD_GETCV(cmd_[1], cmd_[2], cmd_[3]);
					break;

				case CMD_SETCV:
					write("Start notify_CMD_SETCV");
					_Run = false;
					if (notify_CMD_SETCV) {
						notify_CMD_SETCV(cmd_[1], cmd_[2], cmd_[3], cmd_[4]);
					}
					_Run = true;
					break;

				default:
					sprintf(s, "unsupported command %02X", cmd_[0]);
					write(s);

				}
			}
		} // if (read(cmd_))

		// Only read I/O in case the RUN flag ist set.
		if (_Run) {
#ifdef RASCII_DEBUG_MSG
			Serial.println(F("RASCII::process, read Inputs"));
#endif
			if (read_RFID) read_RFID();
			if (read_Digital_Input) read_Digital_Input();
			if (read_Analog_Input) read_Analog_Input();
			if (read_State) read_State();
		}
	}
}


