/*  Arduino Library for the Rocrail RASCII protocol.
Copyright (c) 2018 Andreas Zogg	<don_funk@bluewin.ch>
Copyright (c) 2016 Rob Versluis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Rob Versluis Feburary 14, 2016
Forked by Andreas Zogg, September 26, 2018.

RASCII-Arduino - Version 1.0.1
*/

// #define RASCII_EXTENDED_FUNCS

#ifndef RASCII_h
#define RASCII_h

#include "Arduino.h"

// Prefixes
#define CMD '@'
#define EVT '$'
#define INF '#'
#define RSP '&'

// Commands
#define CMD_INFO      0
#define CMD_START     1
#define CMD_STOP      2
#define CMD_GETCONF   3
#define CMD_SETCONF   4
#define CMD_RESETCONF 5
#define CMD_GETCV     6
#define CMD_SETCV     7
#define CMD_DOUT      10
#define CMD_AOUT      11
#define CMD_ACK       127

// Events
#define EVT_OCC   1
#define EVT_RFID  2
#define EVT_DIN   3
#define EVT_AIN   4
#define EVT_DSEN  5
#define EVT_ASEN  6
#define EVT_STATE        12

#ifdef RASCII_EXTENDED_FUNCS
struct QueueItem {
	byte type;
	byte evt;
	byte len;
	byte data[32];
};

#define QUEUESIZE 10
#endif

class RASCII
{
	bool m_bSoftwareSerial;
	uint8_t  m_iSoftSerialRX;        //Serial Receive pin
	uint8_t  m_iSoftSerialTX;        //Serial Transmit pin
	uint8_t  m_iSoftSerialTxControl; //RS485 Direction control

#ifdef RASCII_EXTENDED_FUNCS
	QueueItem Queue[QUEUESIZE];
#endif

public:
	RASCII(bool softserial = false, uint8_t rxPin = 0, uint8_t txPin = 0, uint8_t txCtrl = 0);
	void begin(long int baudrate);
	bool read(byte* cmd);
	void write(const char* remark);
	void write(byte evt, byte* data, byte len, byte type);

#ifdef RASCII_EXTENDED_FUNCS
	void transmit(bool txrx); // true=Tx, false=Rx
	void queuePut(byte evt, byte* data, byte len, byte type);
	void queuePut(const char* remark);
	void queueWrite();
#endif

	void startRun();

	void stopRun();

	void process(byte boardaddr_);
private:
	byte HexA2Byte(const char* s);
	char* Byte2HexA(byte b, char* c);
	byte strLen(const char* s);
	bool _Run = false;
	uint32_t _previousMillis = 0;
};

extern void notify_CMD_INFO()  __attribute__((weak));
extern void notify_CMD_GETCONF()  __attribute__((weak));
extern void notify_CMD_SETCONF(byte* cmd)  __attribute__((weak));
extern void notify_CMD_RESETCONF()  __attribute__((weak));
extern void notify_CMD_GETCV(byte addr, byte CVHigh, byte CVLow)  __attribute__((weak));
extern void notify_CMD_SETCV(byte addr, byte CVHigh, byte CVLow, byte value)  __attribute__((weak));
extern void notify_CMD_DOUT(byte* cmd)  __attribute__((weak));
extern void notify_CMD_AOUT(byte* cmd)  __attribute__((weak));

extern void read_RFID()   __attribute__((weak));
extern void read_Digital_Input()   __attribute__((weak));
extern void read_Analog_Input()   __attribute__((weak));
extern void read_State()   __attribute__((weak));

#endif
