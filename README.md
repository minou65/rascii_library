Arduino Library for the Rocrail RASCII protocol.  
Copyright (c) 2018 Andreas Zogg  
Copyright (c) 2016 Rob Versluis  
  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.  

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.  

Created by Rob Versluis Feburary 14, 2016  
Forked by Andreas Zogg, September 26, 2018.  

RASCII-Arduino - Version 1.0.0  

For more details see  
https://wiki.rocrail.net/doku.php?id=rascii-en  
https://wiki.rocrail.net/doku.php?id=userpages#rascii  
