#ifndef TEST_h
#define TEST_h

#define PORT_IN       INPUT         // 0x00
#define PORT_OUT      OUTPUT        // 0x01
#define PORT_INPULL   INPUT_PULLUP  // 0x02
#define PORT_SEN      3             // 0x03
#define PORT_RES      4             // 0x04

#define ANA_IN        1
#define ANA_SEN       2
#define ANA_DIN       3
#define ANA_DOUT      4
#define ANA_DSEN      5
#define ANA_RES       6

#define RFID_ACT      1

#define TYPE_UNO  0
#define TYPE_MEGA 1

struct BoardSetup {     // CV -> In Rocrail + 1
	byte addr;            // 0
	byte analogport[16];  // 1...16
	byte digitalport[54]; // 17...70
	byte rfid[8];         // 71...78
	byte rs485;           // 79
	byte master;          // 80
};

// RFID SPI
#define RST_PIN   9
#define SS_PIN    10


#endif
