/*
 Name:		Example.ino
 Created:	10/7/2018 10:39:11 AM
 Author:	andy
*/

#include "RASCII.h"
#include <EEPROM.h>
#include "Example.h"

RASCII rascii(false);
uint8_t myaddr = 255;

// The board configuration structure.
BoardSetup boardSetup;


byte DigVal[14];
byte NrDigital = 14;


// the setup function runs once when you press reset or power the board
void setup() {

	EEPROM.get(0, boardSetup);
	initIO();

	// Start RASCII
	rascii.begin(57600);

}

// the loop function runs over and over again until power down or reset
void loop() {
	// Process the RASCII
	rascii.process(myaddr);

	// Process some other stuff
}

// Set a digital Output
extern void notify_CMD_DOUT(byte* cmd) {

	//byte addr = cmd[1];
	//byte port = cmd[2];
	//byte value = cmd[3];

	if (cmd[2] < 100 && boardSetup.digitalport[cmd[2]] != PORT_RES)
		digitalWrite(cmd[2], cmd[3] ? HIGH : LOW);
}

// Send the configuration to the host.
extern void notify_CMD_GETCONF() {
	byte conf[128];
	memcpy(conf, &boardSetup, sizeof(boardSetup));
	rascii.write(CMD_GETCONF, conf, sizeof(boardSetup), RSP);
}

// Reset the complete board configuration to zero.
extern void notify_CMD_RESETCONF() {
	memset(&boardSetup, 0, sizeof(boardSetup));
	EEPROM.put(0, boardSetup);
	initIO();
}

// Send a CV value to the host.
extern void notify_CMD_GETCV(byte addr, byte CV) {
	byte conf[32];
	byte* p = (byte*)&boardSetup;
	conf[0] = CV;
	conf[1] = p[CV];
	rascii.write(CMD_GETCV, conf, 2, RSP);
}

// Change a CV value of the board configuration.
extern void notify_CMD_SETCV(byte addr, byte CV, byte value) {
	char s[32];
	byte* p = (byte*)&boardSetup;
	p[CV] = value;
	sprintf(s, "set cv %d to %d", CV, value);
	rascii.write(s);
	EEPROM.write((int)&p[CV], value);
	initIO();
}

void initIO() {
	for (byte i = 0; i < NrDigital; i++) {
		DigVal[i] = 0;
		if (boardSetup.digitalport[i] == PORT_OUT)
			pinMode(i, OUTPUT);
		else if (boardSetup.digitalport[i] == PORT_IN)
			pinMode(i, INPUT);
		else if (boardSetup.digitalport[i] == PORT_INPULL)
			pinMode(i, INPUT_PULLUP);
	}
}

